#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPainter>
#include <QRect>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QDebug>
#include <QRadioButton>
#include <QPlainTextEdit>
#include <QFileDialog>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:


protected:
    void paintEvent(QPaintEvent *);

private:
    Ui::MainWindow *ui;
    int maxPosts = 18;
    QJsonArray data_taker;
    QJsonArray data_txt;
    QJsonArray data_colors;
    QJsonArray data_time;

    bool save = false;

    QString wallpaper;
};

#endif // MAINWINDOW_H
