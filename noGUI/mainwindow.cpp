#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    wallpaper = "back.png";


      QString val;

      QFile file;
      file.setFileName("config.json");
      file.open(QIODevice::ReadOnly | QIODevice::Text);
      val = file.readAll();
      file.close();

      QJsonDocument d = QJsonDocument::fromJson(val.toUtf8());
      QJsonObject sett2 = d.object();
      QJsonValue value = sett2.value(QString("desktop"));
      QJsonObject item = value.toObject();

      QJsonValue version = item["version"];
      qWarning() << "version:" << version.toString();

      data_taker = item["taker"].toArray();
      data_txt = item["task"].toArray();
      data_colors = item["colors"].toArray();
      data_time = item["time"].toArray();


      save = true;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *){

    if(save)
    {
        QImage resultImage(wallpaper);
        QImage sourceImage(wallpaper);

        QPainter painter(&resultImage);
        painter.setCompositionMode(QPainter::CompositionMode_Source);
        painter.fillRect(resultImage.rect(), Qt::transparent);
        painter.setCompositionMode(QPainter::CompositionMode_SourceOver);

         QImage destinationImage;
        int x_impar = 1406;
        int y_impar = 34;

        int x_par = 1728;
        int y_par = 34;
        QFont font;


        for(int i=0;i<data_txt.count();i++)
        {
            if(data_colors[i].toString()=="red") destinationImage.load(":/post_red.png");
            else if(data_colors[i].toString()=="green") destinationImage.load(":/post_green.png");
            else if(data_colors[i].toString()=="blue") destinationImage.load(":/post_blue.png");
            else  destinationImage.load(":/post_yellow.png");
            if(i%2)
            {
                painter.drawImage(x_par, y_par, destinationImage);
                font.setPixelSize(16);
                font.setBold(true);
                painter.setFont(font);
                painter.drawText(x_par+10,y_par+30,data_taker[i].toString());
                font.setBold(false);
                painter.setFont(font);
                painter.drawText(x_par+10,y_par+50,data_txt[i].toString());
                painter.drawText(x_par+10,y_par+75,data_time[i].toString());
                y_par = y_par + 121;
            }
            else
            {
                painter.drawImage(x_impar, y_impar, destinationImage);
                font.setPixelSize(16);
                font.setBold(true);
                painter.setFont(font);
                painter.drawText(x_impar+10,y_impar+30,data_taker[i].toString());
                font.setBold(false);
                painter.setFont(font);
                painter.drawText(x_impar+10,y_impar+50,data_txt[i].toString());
                painter.drawText(x_impar+10,y_impar+75,data_time[i].toString());
                y_impar = y_impar + 121;
            }
        }

        painter.setCompositionMode(QPainter::CompositionMode_DestinationOver);
        painter.drawImage(resultImage.rect(), sourceImage);

        painter.setCompositionMode(QPainter::CompositionMode_DestinationOver);
        painter.fillRect(resultImage.rect(), Qt::white);
        painter.end();
        resultImage.save("result_image.png");

        save = false;

        QCoreApplication::quit();
    }

}

