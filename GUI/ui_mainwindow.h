/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QWidget *verticalLayoutWidget_11;
    QVBoxLayout *BOX_1;
    QPlainTextEdit *TB_1;
    QHBoxLayout *HR_1;
    QRadioButton *RAY_1;
    QRadioButton *RAG_1;
    QRadioButton *RAR_1;
    QRadioButton *RAB_1;
    QWidget *verticalLayoutWidget_12;
    QVBoxLayout *BOX_2;
    QPlainTextEdit *TB_2;
    QHBoxLayout *HR_2;
    QRadioButton *RAY_2;
    QRadioButton *RAG_2;
    QRadioButton *RAR_2;
    QRadioButton *RAB_2;
    QWidget *verticalLayoutWidget_13;
    QVBoxLayout *BOX_3;
    QPlainTextEdit *TB_3;
    QHBoxLayout *HR_3;
    QRadioButton *RAY_3;
    QRadioButton *RAG_3;
    QRadioButton *RAR_3;
    QRadioButton *RAB_3;
    QWidget *verticalLayoutWidget_14;
    QVBoxLayout *BOX_4;
    QPlainTextEdit *TB_4;
    QHBoxLayout *HR_4;
    QRadioButton *RAY_4;
    QRadioButton *RAG_4;
    QRadioButton *RAR_4;
    QRadioButton *RAB_4;
    QWidget *verticalLayoutWidget_15;
    QVBoxLayout *BOX_5;
    QPlainTextEdit *TB_5;
    QHBoxLayout *HR_5;
    QRadioButton *RAY_5;
    QRadioButton *RAG_5;
    QRadioButton *RAR_5;
    QRadioButton *RAB_5;
    QWidget *verticalLayoutWidget_16;
    QVBoxLayout *BOX_6;
    QPlainTextEdit *TB_6;
    QHBoxLayout *HR_6;
    QRadioButton *RAY_6;
    QRadioButton *RAG_6;
    QRadioButton *RAR_6;
    QRadioButton *RAB_6;
    QWidget *verticalLayoutWidget_17;
    QVBoxLayout *BOX_7;
    QPlainTextEdit *TB_7;
    QHBoxLayout *HR_7;
    QRadioButton *RAY_7;
    QRadioButton *RAG_7;
    QRadioButton *RAR_7;
    QRadioButton *RAB_7;
    QWidget *verticalLayoutWidget_18;
    QVBoxLayout *BOX_8;
    QPlainTextEdit *TB_8;
    QHBoxLayout *HR_8;
    QRadioButton *RAY_8;
    QRadioButton *RAG_8;
    QRadioButton *RAR_8;
    QRadioButton *RAB_8;
    QWidget *verticalLayoutWidget_19;
    QVBoxLayout *BOX_9;
    QPlainTextEdit *TB_9;
    QHBoxLayout *HR_9;
    QRadioButton *RAY_9;
    QRadioButton *RAG_9;
    QRadioButton *RAR_9;
    QRadioButton *RAB_9;
    QWidget *verticalLayoutWidget_20;
    QVBoxLayout *BOX_10;
    QPlainTextEdit *TB_10;
    QHBoxLayout *HR_10;
    QRadioButton *RAY_10;
    QRadioButton *RAG_10;
    QRadioButton *RAR_10;
    QRadioButton *RAB_10;
    QWidget *verticalLayoutWidget_21;
    QVBoxLayout *BOX_11;
    QPlainTextEdit *TB_11;
    QHBoxLayout *HR_11;
    QRadioButton *RAY_11;
    QRadioButton *RAG_11;
    QRadioButton *RAR_11;
    QRadioButton *RAB_11;
    QWidget *verticalLayoutWidget_22;
    QVBoxLayout *BOX_12;
    QPlainTextEdit *TB_12;
    QHBoxLayout *HR_12;
    QRadioButton *RAY_12;
    QRadioButton *RAG_12;
    QRadioButton *RAR_12;
    QRadioButton *RAB_12;
    QWidget *verticalLayoutWidget_23;
    QVBoxLayout *BOX_13;
    QPlainTextEdit *TB_13;
    QHBoxLayout *HR_13;
    QRadioButton *RAY_13;
    QRadioButton *RAG_13;
    QRadioButton *RAR_13;
    QRadioButton *RAB_13;
    QWidget *verticalLayoutWidget_24;
    QVBoxLayout *BOX_14;
    QPlainTextEdit *TB_14;
    QHBoxLayout *HR_14;
    QRadioButton *RAY_14;
    QRadioButton *RAG_14;
    QRadioButton *RAR_14;
    QRadioButton *RAB_14;
    QWidget *verticalLayoutWidget_25;
    QVBoxLayout *BOX_15;
    QPlainTextEdit *TB_15;
    QHBoxLayout *HR_15;
    QRadioButton *RAY_15;
    QRadioButton *RAG_15;
    QRadioButton *RAR_15;
    QRadioButton *RAB_15;
    QWidget *verticalLayoutWidget_26;
    QVBoxLayout *BOX_16;
    QPlainTextEdit *TB_16;
    QHBoxLayout *HR_16;
    QRadioButton *RAY_16;
    QRadioButton *RAG_16;
    QRadioButton *RAR_16;
    QRadioButton *RAB_16;
    QWidget *verticalLayoutWidget_27;
    QVBoxLayout *BOX_17;
    QPlainTextEdit *TB_17;
    QHBoxLayout *HR_17;
    QRadioButton *RAY_17;
    QRadioButton *RAG_17;
    QRadioButton *RAR_17;
    QRadioButton *RAB_17;
    QWidget *verticalLayoutWidget_28;
    QVBoxLayout *BOX_18;
    QPlainTextEdit *TB_18;
    QHBoxLayout *HR_18;
    QRadioButton *RAY_18;
    QRadioButton *RAG_18;
    QRadioButton *RAR_18;
    QRadioButton *RAB_18;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *bt_image;
    QPushButton *bt_save;
    QPushButton *bt_close;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1920, 1080);
        MainWindow->setMinimumSize(QSize(1920, 1080));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/logo.ico"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        MainWindow->setStyleSheet(QString::fromUtf8(""));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        centralWidget->setStyleSheet(QString::fromUtf8(""));
        verticalLayoutWidget_11 = new QWidget(centralWidget);
        verticalLayoutWidget_11->setObjectName(QString::fromUtf8("verticalLayoutWidget_11"));
        verticalLayoutWidget_11->setGeometry(QRect(1330, 30, 268, 103));
        BOX_1 = new QVBoxLayout(verticalLayoutWidget_11);
        BOX_1->setSpacing(6);
        BOX_1->setContentsMargins(11, 11, 11, 11);
        BOX_1->setObjectName(QString::fromUtf8("BOX_1"));
        BOX_1->setContentsMargins(0, 0, 0, 0);
        TB_1 = new QPlainTextEdit(verticalLayoutWidget_11);
        TB_1->setObjectName(QString::fromUtf8("TB_1"));
        TB_1->setStyleSheet(QString::fromUtf8(""));

        BOX_1->addWidget(TB_1);

        HR_1 = new QHBoxLayout();
        HR_1->setSpacing(6);
        HR_1->setObjectName(QString::fromUtf8("HR_1"));
        RAY_1 = new QRadioButton(verticalLayoutWidget_11);
        RAY_1->setObjectName(QString::fromUtf8("RAY_1"));
        RAY_1->setStyleSheet(QString::fromUtf8(""));
        RAY_1->setChecked(true);

        HR_1->addWidget(RAY_1);

        RAG_1 = new QRadioButton(verticalLayoutWidget_11);
        RAG_1->setObjectName(QString::fromUtf8("RAG_1"));
        RAG_1->setStyleSheet(QString::fromUtf8(""));

        HR_1->addWidget(RAG_1);

        RAR_1 = new QRadioButton(verticalLayoutWidget_11);
        RAR_1->setObjectName(QString::fromUtf8("RAR_1"));
        RAR_1->setStyleSheet(QString::fromUtf8(""));

        HR_1->addWidget(RAR_1);

        RAB_1 = new QRadioButton(verticalLayoutWidget_11);
        RAB_1->setObjectName(QString::fromUtf8("RAB_1"));
        RAB_1->setStyleSheet(QString::fromUtf8(""));

        HR_1->addWidget(RAB_1);


        BOX_1->addLayout(HR_1);

        verticalLayoutWidget_12 = new QWidget(centralWidget);
        verticalLayoutWidget_12->setObjectName(QString::fromUtf8("verticalLayoutWidget_12"));
        verticalLayoutWidget_12->setGeometry(QRect(1630, 30, 268, 103));
        BOX_2 = new QVBoxLayout(verticalLayoutWidget_12);
        BOX_2->setSpacing(6);
        BOX_2->setContentsMargins(11, 11, 11, 11);
        BOX_2->setObjectName(QString::fromUtf8("BOX_2"));
        BOX_2->setContentsMargins(0, 0, 0, 0);
        TB_2 = new QPlainTextEdit(verticalLayoutWidget_12);
        TB_2->setObjectName(QString::fromUtf8("TB_2"));
        TB_2->setStyleSheet(QString::fromUtf8(""));

        BOX_2->addWidget(TB_2);

        HR_2 = new QHBoxLayout();
        HR_2->setSpacing(6);
        HR_2->setObjectName(QString::fromUtf8("HR_2"));
        RAY_2 = new QRadioButton(verticalLayoutWidget_12);
        RAY_2->setObjectName(QString::fromUtf8("RAY_2"));
        RAY_2->setStyleSheet(QString::fromUtf8(""));
        RAY_2->setChecked(false);

        HR_2->addWidget(RAY_2);

        RAG_2 = new QRadioButton(verticalLayoutWidget_12);
        RAG_2->setObjectName(QString::fromUtf8("RAG_2"));
        RAG_2->setStyleSheet(QString::fromUtf8(""));

        HR_2->addWidget(RAG_2);

        RAR_2 = new QRadioButton(verticalLayoutWidget_12);
        RAR_2->setObjectName(QString::fromUtf8("RAR_2"));
        RAR_2->setStyleSheet(QString::fromUtf8(""));
        RAR_2->setChecked(true);

        HR_2->addWidget(RAR_2);

        RAB_2 = new QRadioButton(verticalLayoutWidget_12);
        RAB_2->setObjectName(QString::fromUtf8("RAB_2"));
        RAB_2->setStyleSheet(QString::fromUtf8(""));

        HR_2->addWidget(RAB_2);


        BOX_2->addLayout(HR_2);

        verticalLayoutWidget_13 = new QWidget(centralWidget);
        verticalLayoutWidget_13->setObjectName(QString::fromUtf8("verticalLayoutWidget_13"));
        verticalLayoutWidget_13->setGeometry(QRect(1330, 140, 268, 103));
        BOX_3 = new QVBoxLayout(verticalLayoutWidget_13);
        BOX_3->setSpacing(6);
        BOX_3->setContentsMargins(11, 11, 11, 11);
        BOX_3->setObjectName(QString::fromUtf8("BOX_3"));
        BOX_3->setContentsMargins(0, 0, 0, 0);
        TB_3 = new QPlainTextEdit(verticalLayoutWidget_13);
        TB_3->setObjectName(QString::fromUtf8("TB_3"));
        TB_3->setStyleSheet(QString::fromUtf8(""));

        BOX_3->addWidget(TB_3);

        HR_3 = new QHBoxLayout();
        HR_3->setSpacing(6);
        HR_3->setObjectName(QString::fromUtf8("HR_3"));
        RAY_3 = new QRadioButton(verticalLayoutWidget_13);
        RAY_3->setObjectName(QString::fromUtf8("RAY_3"));
        RAY_3->setStyleSheet(QString::fromUtf8(""));
        RAY_3->setChecked(true);

        HR_3->addWidget(RAY_3);

        RAG_3 = new QRadioButton(verticalLayoutWidget_13);
        RAG_3->setObjectName(QString::fromUtf8("RAG_3"));
        RAG_3->setStyleSheet(QString::fromUtf8(""));

        HR_3->addWidget(RAG_3);

        RAR_3 = new QRadioButton(verticalLayoutWidget_13);
        RAR_3->setObjectName(QString::fromUtf8("RAR_3"));
        RAR_3->setStyleSheet(QString::fromUtf8(""));

        HR_3->addWidget(RAR_3);

        RAB_3 = new QRadioButton(verticalLayoutWidget_13);
        RAB_3->setObjectName(QString::fromUtf8("RAB_3"));
        RAB_3->setStyleSheet(QString::fromUtf8(""));

        HR_3->addWidget(RAB_3);


        BOX_3->addLayout(HR_3);

        verticalLayoutWidget_14 = new QWidget(centralWidget);
        verticalLayoutWidget_14->setObjectName(QString::fromUtf8("verticalLayoutWidget_14"));
        verticalLayoutWidget_14->setGeometry(QRect(1630, 140, 268, 103));
        BOX_4 = new QVBoxLayout(verticalLayoutWidget_14);
        BOX_4->setSpacing(6);
        BOX_4->setContentsMargins(11, 11, 11, 11);
        BOX_4->setObjectName(QString::fromUtf8("BOX_4"));
        BOX_4->setContentsMargins(0, 0, 0, 0);
        TB_4 = new QPlainTextEdit(verticalLayoutWidget_14);
        TB_4->setObjectName(QString::fromUtf8("TB_4"));
        TB_4->setStyleSheet(QString::fromUtf8(""));

        BOX_4->addWidget(TB_4);

        HR_4 = new QHBoxLayout();
        HR_4->setSpacing(6);
        HR_4->setObjectName(QString::fromUtf8("HR_4"));
        RAY_4 = new QRadioButton(verticalLayoutWidget_14);
        RAY_4->setObjectName(QString::fromUtf8("RAY_4"));
        RAY_4->setStyleSheet(QString::fromUtf8(""));
        RAY_4->setChecked(true);

        HR_4->addWidget(RAY_4);

        RAG_4 = new QRadioButton(verticalLayoutWidget_14);
        RAG_4->setObjectName(QString::fromUtf8("RAG_4"));
        RAG_4->setStyleSheet(QString::fromUtf8(""));

        HR_4->addWidget(RAG_4);

        RAR_4 = new QRadioButton(verticalLayoutWidget_14);
        RAR_4->setObjectName(QString::fromUtf8("RAR_4"));
        RAR_4->setStyleSheet(QString::fromUtf8(""));

        HR_4->addWidget(RAR_4);

        RAB_4 = new QRadioButton(verticalLayoutWidget_14);
        RAB_4->setObjectName(QString::fromUtf8("RAB_4"));
        RAB_4->setStyleSheet(QString::fromUtf8(""));

        HR_4->addWidget(RAB_4);


        BOX_4->addLayout(HR_4);

        verticalLayoutWidget_15 = new QWidget(centralWidget);
        verticalLayoutWidget_15->setObjectName(QString::fromUtf8("verticalLayoutWidget_15"));
        verticalLayoutWidget_15->setGeometry(QRect(1330, 250, 268, 103));
        BOX_5 = new QVBoxLayout(verticalLayoutWidget_15);
        BOX_5->setSpacing(6);
        BOX_5->setContentsMargins(11, 11, 11, 11);
        BOX_5->setObjectName(QString::fromUtf8("BOX_5"));
        BOX_5->setContentsMargins(0, 0, 0, 0);
        TB_5 = new QPlainTextEdit(verticalLayoutWidget_15);
        TB_5->setObjectName(QString::fromUtf8("TB_5"));
        TB_5->setStyleSheet(QString::fromUtf8(""));

        BOX_5->addWidget(TB_5);

        HR_5 = new QHBoxLayout();
        HR_5->setSpacing(6);
        HR_5->setObjectName(QString::fromUtf8("HR_5"));
        RAY_5 = new QRadioButton(verticalLayoutWidget_15);
        RAY_5->setObjectName(QString::fromUtf8("RAY_5"));
        RAY_5->setStyleSheet(QString::fromUtf8(""));
        RAY_5->setChecked(true);

        HR_5->addWidget(RAY_5);

        RAG_5 = new QRadioButton(verticalLayoutWidget_15);
        RAG_5->setObjectName(QString::fromUtf8("RAG_5"));
        RAG_5->setStyleSheet(QString::fromUtf8(""));

        HR_5->addWidget(RAG_5);

        RAR_5 = new QRadioButton(verticalLayoutWidget_15);
        RAR_5->setObjectName(QString::fromUtf8("RAR_5"));
        RAR_5->setStyleSheet(QString::fromUtf8(""));

        HR_5->addWidget(RAR_5);

        RAB_5 = new QRadioButton(verticalLayoutWidget_15);
        RAB_5->setObjectName(QString::fromUtf8("RAB_5"));
        RAB_5->setStyleSheet(QString::fromUtf8(""));

        HR_5->addWidget(RAB_5);


        BOX_5->addLayout(HR_5);

        verticalLayoutWidget_16 = new QWidget(centralWidget);
        verticalLayoutWidget_16->setObjectName(QString::fromUtf8("verticalLayoutWidget_16"));
        verticalLayoutWidget_16->setGeometry(QRect(1630, 250, 268, 103));
        BOX_6 = new QVBoxLayout(verticalLayoutWidget_16);
        BOX_6->setSpacing(6);
        BOX_6->setContentsMargins(11, 11, 11, 11);
        BOX_6->setObjectName(QString::fromUtf8("BOX_6"));
        BOX_6->setContentsMargins(0, 0, 0, 0);
        TB_6 = new QPlainTextEdit(verticalLayoutWidget_16);
        TB_6->setObjectName(QString::fromUtf8("TB_6"));
        TB_6->setStyleSheet(QString::fromUtf8(""));

        BOX_6->addWidget(TB_6);

        HR_6 = new QHBoxLayout();
        HR_6->setSpacing(6);
        HR_6->setObjectName(QString::fromUtf8("HR_6"));
        RAY_6 = new QRadioButton(verticalLayoutWidget_16);
        RAY_6->setObjectName(QString::fromUtf8("RAY_6"));
        RAY_6->setStyleSheet(QString::fromUtf8(""));
        RAY_6->setChecked(true);

        HR_6->addWidget(RAY_6);

        RAG_6 = new QRadioButton(verticalLayoutWidget_16);
        RAG_6->setObjectName(QString::fromUtf8("RAG_6"));
        RAG_6->setStyleSheet(QString::fromUtf8(""));

        HR_6->addWidget(RAG_6);

        RAR_6 = new QRadioButton(verticalLayoutWidget_16);
        RAR_6->setObjectName(QString::fromUtf8("RAR_6"));
        RAR_6->setStyleSheet(QString::fromUtf8(""));

        HR_6->addWidget(RAR_6);

        RAB_6 = new QRadioButton(verticalLayoutWidget_16);
        RAB_6->setObjectName(QString::fromUtf8("RAB_6"));
        RAB_6->setStyleSheet(QString::fromUtf8(""));

        HR_6->addWidget(RAB_6);


        BOX_6->addLayout(HR_6);

        verticalLayoutWidget_17 = new QWidget(centralWidget);
        verticalLayoutWidget_17->setObjectName(QString::fromUtf8("verticalLayoutWidget_17"));
        verticalLayoutWidget_17->setGeometry(QRect(1330, 350, 268, 103));
        BOX_7 = new QVBoxLayout(verticalLayoutWidget_17);
        BOX_7->setSpacing(6);
        BOX_7->setContentsMargins(11, 11, 11, 11);
        BOX_7->setObjectName(QString::fromUtf8("BOX_7"));
        BOX_7->setContentsMargins(0, 0, 0, 0);
        TB_7 = new QPlainTextEdit(verticalLayoutWidget_17);
        TB_7->setObjectName(QString::fromUtf8("TB_7"));
        TB_7->setStyleSheet(QString::fromUtf8(""));

        BOX_7->addWidget(TB_7);

        HR_7 = new QHBoxLayout();
        HR_7->setSpacing(6);
        HR_7->setObjectName(QString::fromUtf8("HR_7"));
        RAY_7 = new QRadioButton(verticalLayoutWidget_17);
        RAY_7->setObjectName(QString::fromUtf8("RAY_7"));
        RAY_7->setStyleSheet(QString::fromUtf8(""));
        RAY_7->setChecked(true);

        HR_7->addWidget(RAY_7);

        RAG_7 = new QRadioButton(verticalLayoutWidget_17);
        RAG_7->setObjectName(QString::fromUtf8("RAG_7"));
        RAG_7->setStyleSheet(QString::fromUtf8(""));

        HR_7->addWidget(RAG_7);

        RAR_7 = new QRadioButton(verticalLayoutWidget_17);
        RAR_7->setObjectName(QString::fromUtf8("RAR_7"));
        RAR_7->setStyleSheet(QString::fromUtf8(""));

        HR_7->addWidget(RAR_7);

        RAB_7 = new QRadioButton(verticalLayoutWidget_17);
        RAB_7->setObjectName(QString::fromUtf8("RAB_7"));
        RAB_7->setStyleSheet(QString::fromUtf8(""));

        HR_7->addWidget(RAB_7);


        BOX_7->addLayout(HR_7);

        verticalLayoutWidget_18 = new QWidget(centralWidget);
        verticalLayoutWidget_18->setObjectName(QString::fromUtf8("verticalLayoutWidget_18"));
        verticalLayoutWidget_18->setGeometry(QRect(1630, 350, 268, 103));
        BOX_8 = new QVBoxLayout(verticalLayoutWidget_18);
        BOX_8->setSpacing(6);
        BOX_8->setContentsMargins(11, 11, 11, 11);
        BOX_8->setObjectName(QString::fromUtf8("BOX_8"));
        BOX_8->setContentsMargins(0, 0, 0, 0);
        TB_8 = new QPlainTextEdit(verticalLayoutWidget_18);
        TB_8->setObjectName(QString::fromUtf8("TB_8"));
        TB_8->setStyleSheet(QString::fromUtf8(""));

        BOX_8->addWidget(TB_8);

        HR_8 = new QHBoxLayout();
        HR_8->setSpacing(6);
        HR_8->setObjectName(QString::fromUtf8("HR_8"));
        RAY_8 = new QRadioButton(verticalLayoutWidget_18);
        RAY_8->setObjectName(QString::fromUtf8("RAY_8"));
        RAY_8->setStyleSheet(QString::fromUtf8(""));
        RAY_8->setChecked(true);

        HR_8->addWidget(RAY_8);

        RAG_8 = new QRadioButton(verticalLayoutWidget_18);
        RAG_8->setObjectName(QString::fromUtf8("RAG_8"));
        RAG_8->setStyleSheet(QString::fromUtf8(""));

        HR_8->addWidget(RAG_8);

        RAR_8 = new QRadioButton(verticalLayoutWidget_18);
        RAR_8->setObjectName(QString::fromUtf8("RAR_8"));
        RAR_8->setStyleSheet(QString::fromUtf8(""));

        HR_8->addWidget(RAR_8);

        RAB_8 = new QRadioButton(verticalLayoutWidget_18);
        RAB_8->setObjectName(QString::fromUtf8("RAB_8"));
        RAB_8->setStyleSheet(QString::fromUtf8(""));

        HR_8->addWidget(RAB_8);


        BOX_8->addLayout(HR_8);

        verticalLayoutWidget_19 = new QWidget(centralWidget);
        verticalLayoutWidget_19->setObjectName(QString::fromUtf8("verticalLayoutWidget_19"));
        verticalLayoutWidget_19->setGeometry(QRect(1330, 460, 268, 103));
        BOX_9 = new QVBoxLayout(verticalLayoutWidget_19);
        BOX_9->setSpacing(6);
        BOX_9->setContentsMargins(11, 11, 11, 11);
        BOX_9->setObjectName(QString::fromUtf8("BOX_9"));
        BOX_9->setContentsMargins(0, 0, 0, 0);
        TB_9 = new QPlainTextEdit(verticalLayoutWidget_19);
        TB_9->setObjectName(QString::fromUtf8("TB_9"));
        TB_9->setStyleSheet(QString::fromUtf8(""));

        BOX_9->addWidget(TB_9);

        HR_9 = new QHBoxLayout();
        HR_9->setSpacing(6);
        HR_9->setObjectName(QString::fromUtf8("HR_9"));
        RAY_9 = new QRadioButton(verticalLayoutWidget_19);
        RAY_9->setObjectName(QString::fromUtf8("RAY_9"));
        RAY_9->setStyleSheet(QString::fromUtf8(""));
        RAY_9->setChecked(true);

        HR_9->addWidget(RAY_9);

        RAG_9 = new QRadioButton(verticalLayoutWidget_19);
        RAG_9->setObjectName(QString::fromUtf8("RAG_9"));
        RAG_9->setStyleSheet(QString::fromUtf8(""));

        HR_9->addWidget(RAG_9);

        RAR_9 = new QRadioButton(verticalLayoutWidget_19);
        RAR_9->setObjectName(QString::fromUtf8("RAR_9"));
        RAR_9->setStyleSheet(QString::fromUtf8(""));

        HR_9->addWidget(RAR_9);

        RAB_9 = new QRadioButton(verticalLayoutWidget_19);
        RAB_9->setObjectName(QString::fromUtf8("RAB_9"));
        RAB_9->setStyleSheet(QString::fromUtf8(""));

        HR_9->addWidget(RAB_9);


        BOX_9->addLayout(HR_9);

        verticalLayoutWidget_20 = new QWidget(centralWidget);
        verticalLayoutWidget_20->setObjectName(QString::fromUtf8("verticalLayoutWidget_20"));
        verticalLayoutWidget_20->setGeometry(QRect(1630, 460, 268, 103));
        BOX_10 = new QVBoxLayout(verticalLayoutWidget_20);
        BOX_10->setSpacing(6);
        BOX_10->setContentsMargins(11, 11, 11, 11);
        BOX_10->setObjectName(QString::fromUtf8("BOX_10"));
        BOX_10->setContentsMargins(0, 0, 0, 0);
        TB_10 = new QPlainTextEdit(verticalLayoutWidget_20);
        TB_10->setObjectName(QString::fromUtf8("TB_10"));
        TB_10->setStyleSheet(QString::fromUtf8(""));

        BOX_10->addWidget(TB_10);

        HR_10 = new QHBoxLayout();
        HR_10->setSpacing(6);
        HR_10->setObjectName(QString::fromUtf8("HR_10"));
        RAY_10 = new QRadioButton(verticalLayoutWidget_20);
        RAY_10->setObjectName(QString::fromUtf8("RAY_10"));
        RAY_10->setStyleSheet(QString::fromUtf8(""));
        RAY_10->setChecked(true);

        HR_10->addWidget(RAY_10);

        RAG_10 = new QRadioButton(verticalLayoutWidget_20);
        RAG_10->setObjectName(QString::fromUtf8("RAG_10"));
        RAG_10->setStyleSheet(QString::fromUtf8(""));

        HR_10->addWidget(RAG_10);

        RAR_10 = new QRadioButton(verticalLayoutWidget_20);
        RAR_10->setObjectName(QString::fromUtf8("RAR_10"));
        RAR_10->setStyleSheet(QString::fromUtf8(""));

        HR_10->addWidget(RAR_10);

        RAB_10 = new QRadioButton(verticalLayoutWidget_20);
        RAB_10->setObjectName(QString::fromUtf8("RAB_10"));
        RAB_10->setStyleSheet(QString::fromUtf8(""));

        HR_10->addWidget(RAB_10);


        BOX_10->addLayout(HR_10);

        verticalLayoutWidget_21 = new QWidget(centralWidget);
        verticalLayoutWidget_21->setObjectName(QString::fromUtf8("verticalLayoutWidget_21"));
        verticalLayoutWidget_21->setGeometry(QRect(1330, 570, 268, 103));
        BOX_11 = new QVBoxLayout(verticalLayoutWidget_21);
        BOX_11->setSpacing(6);
        BOX_11->setContentsMargins(11, 11, 11, 11);
        BOX_11->setObjectName(QString::fromUtf8("BOX_11"));
        BOX_11->setContentsMargins(0, 0, 0, 0);
        TB_11 = new QPlainTextEdit(verticalLayoutWidget_21);
        TB_11->setObjectName(QString::fromUtf8("TB_11"));
        TB_11->setStyleSheet(QString::fromUtf8(""));

        BOX_11->addWidget(TB_11);

        HR_11 = new QHBoxLayout();
        HR_11->setSpacing(6);
        HR_11->setObjectName(QString::fromUtf8("HR_11"));
        RAY_11 = new QRadioButton(verticalLayoutWidget_21);
        RAY_11->setObjectName(QString::fromUtf8("RAY_11"));
        RAY_11->setStyleSheet(QString::fromUtf8(""));
        RAY_11->setChecked(true);

        HR_11->addWidget(RAY_11);

        RAG_11 = new QRadioButton(verticalLayoutWidget_21);
        RAG_11->setObjectName(QString::fromUtf8("RAG_11"));
        RAG_11->setStyleSheet(QString::fromUtf8(""));

        HR_11->addWidget(RAG_11);

        RAR_11 = new QRadioButton(verticalLayoutWidget_21);
        RAR_11->setObjectName(QString::fromUtf8("RAR_11"));
        RAR_11->setStyleSheet(QString::fromUtf8(""));

        HR_11->addWidget(RAR_11);

        RAB_11 = new QRadioButton(verticalLayoutWidget_21);
        RAB_11->setObjectName(QString::fromUtf8("RAB_11"));
        RAB_11->setStyleSheet(QString::fromUtf8(""));

        HR_11->addWidget(RAB_11);


        BOX_11->addLayout(HR_11);

        verticalLayoutWidget_22 = new QWidget(centralWidget);
        verticalLayoutWidget_22->setObjectName(QString::fromUtf8("verticalLayoutWidget_22"));
        verticalLayoutWidget_22->setGeometry(QRect(1630, 570, 268, 103));
        BOX_12 = new QVBoxLayout(verticalLayoutWidget_22);
        BOX_12->setSpacing(6);
        BOX_12->setContentsMargins(11, 11, 11, 11);
        BOX_12->setObjectName(QString::fromUtf8("BOX_12"));
        BOX_12->setContentsMargins(0, 0, 0, 0);
        TB_12 = new QPlainTextEdit(verticalLayoutWidget_22);
        TB_12->setObjectName(QString::fromUtf8("TB_12"));
        TB_12->setStyleSheet(QString::fromUtf8(""));

        BOX_12->addWidget(TB_12);

        HR_12 = new QHBoxLayout();
        HR_12->setSpacing(6);
        HR_12->setObjectName(QString::fromUtf8("HR_12"));
        RAY_12 = new QRadioButton(verticalLayoutWidget_22);
        RAY_12->setObjectName(QString::fromUtf8("RAY_12"));
        RAY_12->setStyleSheet(QString::fromUtf8(""));
        RAY_12->setChecked(true);

        HR_12->addWidget(RAY_12);

        RAG_12 = new QRadioButton(verticalLayoutWidget_22);
        RAG_12->setObjectName(QString::fromUtf8("RAG_12"));
        RAG_12->setStyleSheet(QString::fromUtf8(""));

        HR_12->addWidget(RAG_12);

        RAR_12 = new QRadioButton(verticalLayoutWidget_22);
        RAR_12->setObjectName(QString::fromUtf8("RAR_12"));
        RAR_12->setStyleSheet(QString::fromUtf8(""));

        HR_12->addWidget(RAR_12);

        RAB_12 = new QRadioButton(verticalLayoutWidget_22);
        RAB_12->setObjectName(QString::fromUtf8("RAB_12"));
        RAB_12->setStyleSheet(QString::fromUtf8(""));

        HR_12->addWidget(RAB_12);


        BOX_12->addLayout(HR_12);

        verticalLayoutWidget_23 = new QWidget(centralWidget);
        verticalLayoutWidget_23->setObjectName(QString::fromUtf8("verticalLayoutWidget_23"));
        verticalLayoutWidget_23->setGeometry(QRect(1330, 670, 268, 103));
        BOX_13 = new QVBoxLayout(verticalLayoutWidget_23);
        BOX_13->setSpacing(6);
        BOX_13->setContentsMargins(11, 11, 11, 11);
        BOX_13->setObjectName(QString::fromUtf8("BOX_13"));
        BOX_13->setContentsMargins(0, 0, 0, 0);
        TB_13 = new QPlainTextEdit(verticalLayoutWidget_23);
        TB_13->setObjectName(QString::fromUtf8("TB_13"));
        TB_13->setStyleSheet(QString::fromUtf8(""));

        BOX_13->addWidget(TB_13);

        HR_13 = new QHBoxLayout();
        HR_13->setSpacing(6);
        HR_13->setObjectName(QString::fromUtf8("HR_13"));
        RAY_13 = new QRadioButton(verticalLayoutWidget_23);
        RAY_13->setObjectName(QString::fromUtf8("RAY_13"));
        RAY_13->setStyleSheet(QString::fromUtf8(""));
        RAY_13->setChecked(true);

        HR_13->addWidget(RAY_13);

        RAG_13 = new QRadioButton(verticalLayoutWidget_23);
        RAG_13->setObjectName(QString::fromUtf8("RAG_13"));
        RAG_13->setStyleSheet(QString::fromUtf8(""));

        HR_13->addWidget(RAG_13);

        RAR_13 = new QRadioButton(verticalLayoutWidget_23);
        RAR_13->setObjectName(QString::fromUtf8("RAR_13"));
        RAR_13->setStyleSheet(QString::fromUtf8(""));

        HR_13->addWidget(RAR_13);

        RAB_13 = new QRadioButton(verticalLayoutWidget_23);
        RAB_13->setObjectName(QString::fromUtf8("RAB_13"));
        RAB_13->setStyleSheet(QString::fromUtf8(""));

        HR_13->addWidget(RAB_13);


        BOX_13->addLayout(HR_13);

        verticalLayoutWidget_24 = new QWidget(centralWidget);
        verticalLayoutWidget_24->setObjectName(QString::fromUtf8("verticalLayoutWidget_24"));
        verticalLayoutWidget_24->setGeometry(QRect(1630, 670, 268, 103));
        BOX_14 = new QVBoxLayout(verticalLayoutWidget_24);
        BOX_14->setSpacing(6);
        BOX_14->setContentsMargins(11, 11, 11, 11);
        BOX_14->setObjectName(QString::fromUtf8("BOX_14"));
        BOX_14->setContentsMargins(0, 0, 0, 0);
        TB_14 = new QPlainTextEdit(verticalLayoutWidget_24);
        TB_14->setObjectName(QString::fromUtf8("TB_14"));
        TB_14->setStyleSheet(QString::fromUtf8(""));

        BOX_14->addWidget(TB_14);

        HR_14 = new QHBoxLayout();
        HR_14->setSpacing(6);
        HR_14->setObjectName(QString::fromUtf8("HR_14"));
        RAY_14 = new QRadioButton(verticalLayoutWidget_24);
        RAY_14->setObjectName(QString::fromUtf8("RAY_14"));
        RAY_14->setStyleSheet(QString::fromUtf8(""));
        RAY_14->setChecked(true);

        HR_14->addWidget(RAY_14);

        RAG_14 = new QRadioButton(verticalLayoutWidget_24);
        RAG_14->setObjectName(QString::fromUtf8("RAG_14"));
        RAG_14->setStyleSheet(QString::fromUtf8(""));

        HR_14->addWidget(RAG_14);

        RAR_14 = new QRadioButton(verticalLayoutWidget_24);
        RAR_14->setObjectName(QString::fromUtf8("RAR_14"));
        RAR_14->setStyleSheet(QString::fromUtf8(""));

        HR_14->addWidget(RAR_14);

        RAB_14 = new QRadioButton(verticalLayoutWidget_24);
        RAB_14->setObjectName(QString::fromUtf8("RAB_14"));
        RAB_14->setStyleSheet(QString::fromUtf8(""));

        HR_14->addWidget(RAB_14);


        BOX_14->addLayout(HR_14);

        verticalLayoutWidget_25 = new QWidget(centralWidget);
        verticalLayoutWidget_25->setObjectName(QString::fromUtf8("verticalLayoutWidget_25"));
        verticalLayoutWidget_25->setGeometry(QRect(1330, 780, 268, 103));
        BOX_15 = new QVBoxLayout(verticalLayoutWidget_25);
        BOX_15->setSpacing(6);
        BOX_15->setContentsMargins(11, 11, 11, 11);
        BOX_15->setObjectName(QString::fromUtf8("BOX_15"));
        BOX_15->setContentsMargins(0, 0, 0, 0);
        TB_15 = new QPlainTextEdit(verticalLayoutWidget_25);
        TB_15->setObjectName(QString::fromUtf8("TB_15"));
        TB_15->setStyleSheet(QString::fromUtf8(""));

        BOX_15->addWidget(TB_15);

        HR_15 = new QHBoxLayout();
        HR_15->setSpacing(6);
        HR_15->setObjectName(QString::fromUtf8("HR_15"));
        RAY_15 = new QRadioButton(verticalLayoutWidget_25);
        RAY_15->setObjectName(QString::fromUtf8("RAY_15"));
        RAY_15->setStyleSheet(QString::fromUtf8(""));
        RAY_15->setChecked(true);

        HR_15->addWidget(RAY_15);

        RAG_15 = new QRadioButton(verticalLayoutWidget_25);
        RAG_15->setObjectName(QString::fromUtf8("RAG_15"));
        RAG_15->setStyleSheet(QString::fromUtf8(""));

        HR_15->addWidget(RAG_15);

        RAR_15 = new QRadioButton(verticalLayoutWidget_25);
        RAR_15->setObjectName(QString::fromUtf8("RAR_15"));
        RAR_15->setStyleSheet(QString::fromUtf8(""));

        HR_15->addWidget(RAR_15);

        RAB_15 = new QRadioButton(verticalLayoutWidget_25);
        RAB_15->setObjectName(QString::fromUtf8("RAB_15"));
        RAB_15->setStyleSheet(QString::fromUtf8(""));

        HR_15->addWidget(RAB_15);


        BOX_15->addLayout(HR_15);

        verticalLayoutWidget_26 = new QWidget(centralWidget);
        verticalLayoutWidget_26->setObjectName(QString::fromUtf8("verticalLayoutWidget_26"));
        verticalLayoutWidget_26->setGeometry(QRect(1630, 780, 268, 103));
        BOX_16 = new QVBoxLayout(verticalLayoutWidget_26);
        BOX_16->setSpacing(6);
        BOX_16->setContentsMargins(11, 11, 11, 11);
        BOX_16->setObjectName(QString::fromUtf8("BOX_16"));
        BOX_16->setContentsMargins(0, 0, 0, 0);
        TB_16 = new QPlainTextEdit(verticalLayoutWidget_26);
        TB_16->setObjectName(QString::fromUtf8("TB_16"));
        TB_16->setStyleSheet(QString::fromUtf8(""));

        BOX_16->addWidget(TB_16);

        HR_16 = new QHBoxLayout();
        HR_16->setSpacing(6);
        HR_16->setObjectName(QString::fromUtf8("HR_16"));
        RAY_16 = new QRadioButton(verticalLayoutWidget_26);
        RAY_16->setObjectName(QString::fromUtf8("RAY_16"));
        RAY_16->setStyleSheet(QString::fromUtf8(""));
        RAY_16->setChecked(true);

        HR_16->addWidget(RAY_16);

        RAG_16 = new QRadioButton(verticalLayoutWidget_26);
        RAG_16->setObjectName(QString::fromUtf8("RAG_16"));
        RAG_16->setStyleSheet(QString::fromUtf8(""));

        HR_16->addWidget(RAG_16);

        RAR_16 = new QRadioButton(verticalLayoutWidget_26);
        RAR_16->setObjectName(QString::fromUtf8("RAR_16"));
        RAR_16->setStyleSheet(QString::fromUtf8(""));

        HR_16->addWidget(RAR_16);

        RAB_16 = new QRadioButton(verticalLayoutWidget_26);
        RAB_16->setObjectName(QString::fromUtf8("RAB_16"));
        RAB_16->setStyleSheet(QString::fromUtf8(""));

        HR_16->addWidget(RAB_16);


        BOX_16->addLayout(HR_16);

        verticalLayoutWidget_27 = new QWidget(centralWidget);
        verticalLayoutWidget_27->setObjectName(QString::fromUtf8("verticalLayoutWidget_27"));
        verticalLayoutWidget_27->setGeometry(QRect(1330, 890, 268, 103));
        BOX_17 = new QVBoxLayout(verticalLayoutWidget_27);
        BOX_17->setSpacing(6);
        BOX_17->setContentsMargins(11, 11, 11, 11);
        BOX_17->setObjectName(QString::fromUtf8("BOX_17"));
        BOX_17->setContentsMargins(0, 0, 0, 0);
        TB_17 = new QPlainTextEdit(verticalLayoutWidget_27);
        TB_17->setObjectName(QString::fromUtf8("TB_17"));
        TB_17->setStyleSheet(QString::fromUtf8(""));

        BOX_17->addWidget(TB_17);

        HR_17 = new QHBoxLayout();
        HR_17->setSpacing(6);
        HR_17->setObjectName(QString::fromUtf8("HR_17"));
        RAY_17 = new QRadioButton(verticalLayoutWidget_27);
        RAY_17->setObjectName(QString::fromUtf8("RAY_17"));
        RAY_17->setStyleSheet(QString::fromUtf8(""));
        RAY_17->setChecked(true);

        HR_17->addWidget(RAY_17);

        RAG_17 = new QRadioButton(verticalLayoutWidget_27);
        RAG_17->setObjectName(QString::fromUtf8("RAG_17"));
        RAG_17->setStyleSheet(QString::fromUtf8(""));

        HR_17->addWidget(RAG_17);

        RAR_17 = new QRadioButton(verticalLayoutWidget_27);
        RAR_17->setObjectName(QString::fromUtf8("RAR_17"));
        RAR_17->setStyleSheet(QString::fromUtf8(""));

        HR_17->addWidget(RAR_17);

        RAB_17 = new QRadioButton(verticalLayoutWidget_27);
        RAB_17->setObjectName(QString::fromUtf8("RAB_17"));
        RAB_17->setStyleSheet(QString::fromUtf8(""));

        HR_17->addWidget(RAB_17);


        BOX_17->addLayout(HR_17);

        verticalLayoutWidget_28 = new QWidget(centralWidget);
        verticalLayoutWidget_28->setObjectName(QString::fromUtf8("verticalLayoutWidget_28"));
        verticalLayoutWidget_28->setGeometry(QRect(1630, 890, 268, 103));
        BOX_18 = new QVBoxLayout(verticalLayoutWidget_28);
        BOX_18->setSpacing(6);
        BOX_18->setContentsMargins(11, 11, 11, 11);
        BOX_18->setObjectName(QString::fromUtf8("BOX_18"));
        BOX_18->setContentsMargins(0, 0, 0, 0);
        TB_18 = new QPlainTextEdit(verticalLayoutWidget_28);
        TB_18->setObjectName(QString::fromUtf8("TB_18"));
        TB_18->setStyleSheet(QString::fromUtf8(""));

        BOX_18->addWidget(TB_18);

        HR_18 = new QHBoxLayout();
        HR_18->setSpacing(6);
        HR_18->setObjectName(QString::fromUtf8("HR_18"));
        RAY_18 = new QRadioButton(verticalLayoutWidget_28);
        RAY_18->setObjectName(QString::fromUtf8("RAY_18"));
        RAY_18->setStyleSheet(QString::fromUtf8(""));
        RAY_18->setChecked(true);

        HR_18->addWidget(RAY_18);

        RAG_18 = new QRadioButton(verticalLayoutWidget_28);
        RAG_18->setObjectName(QString::fromUtf8("RAG_18"));
        RAG_18->setStyleSheet(QString::fromUtf8(""));

        HR_18->addWidget(RAG_18);

        RAR_18 = new QRadioButton(verticalLayoutWidget_28);
        RAR_18->setObjectName(QString::fromUtf8("RAR_18"));
        RAR_18->setStyleSheet(QString::fromUtf8(""));

        HR_18->addWidget(RAR_18);

        RAB_18 = new QRadioButton(verticalLayoutWidget_28);
        RAB_18->setObjectName(QString::fromUtf8("RAB_18"));
        RAB_18->setStyleSheet(QString::fromUtf8(""));

        HR_18->addWidget(RAB_18);


        BOX_18->addLayout(HR_18);

        horizontalLayoutWidget = new QWidget(centralWidget);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(1430, 1000, 361, 41));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        bt_image = new QPushButton(horizontalLayoutWidget);
        bt_image->setObjectName(QString::fromUtf8("bt_image"));
        bt_image->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 150, 0);\n"
"color: rgb(238, 238, 236);"));

        horizontalLayout->addWidget(bt_image);

        bt_save = new QPushButton(horizontalLayoutWidget);
        bt_save->setObjectName(QString::fromUtf8("bt_save"));
        bt_save->setStyleSheet(QString::fromUtf8("background-color: rgb(25, 84, 48);\n"
"color: rgb(238, 238, 236);"));

        horizontalLayout->addWidget(bt_save);

        bt_close = new QPushButton(horizontalLayoutWidget);
        bt_close->setObjectName(QString::fromUtf8("bt_close"));
        bt_close->setStyleSheet(QString::fromUtf8("background-color: rgb(164, 0, 0);\n"
"color: rgb(238, 238, 236);"));

        horizontalLayout->addWidget(bt_close);

        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "DeskApp", nullptr));
        RAY_1->setText(QApplication::translate("MainWindow", "Yellow", nullptr));
        RAG_1->setText(QApplication::translate("MainWindow", "Green", nullptr));
        RAR_1->setText(QApplication::translate("MainWindow", "Red", nullptr));
        RAB_1->setText(QApplication::translate("MainWindow", "Blue", nullptr));
        RAY_2->setText(QApplication::translate("MainWindow", "Yellow", nullptr));
        RAG_2->setText(QApplication::translate("MainWindow", "Green", nullptr));
        RAR_2->setText(QApplication::translate("MainWindow", "Red", nullptr));
        RAB_2->setText(QApplication::translate("MainWindow", "Blue", nullptr));
        RAY_3->setText(QApplication::translate("MainWindow", "Yellow", nullptr));
        RAG_3->setText(QApplication::translate("MainWindow", "Green", nullptr));
        RAR_3->setText(QApplication::translate("MainWindow", "Red", nullptr));
        RAB_3->setText(QApplication::translate("MainWindow", "Blue", nullptr));
        RAY_4->setText(QApplication::translate("MainWindow", "Yellow", nullptr));
        RAG_4->setText(QApplication::translate("MainWindow", "Green", nullptr));
        RAR_4->setText(QApplication::translate("MainWindow", "Red", nullptr));
        RAB_4->setText(QApplication::translate("MainWindow", "Blue", nullptr));
        RAY_5->setText(QApplication::translate("MainWindow", "Yellow", nullptr));
        RAG_5->setText(QApplication::translate("MainWindow", "Green", nullptr));
        RAR_5->setText(QApplication::translate("MainWindow", "Red", nullptr));
        RAB_5->setText(QApplication::translate("MainWindow", "Blue", nullptr));
        RAY_6->setText(QApplication::translate("MainWindow", "Yellow", nullptr));
        RAG_6->setText(QApplication::translate("MainWindow", "Green", nullptr));
        RAR_6->setText(QApplication::translate("MainWindow", "Red", nullptr));
        RAB_6->setText(QApplication::translate("MainWindow", "Blue", nullptr));
        RAY_7->setText(QApplication::translate("MainWindow", "Yellow", nullptr));
        RAG_7->setText(QApplication::translate("MainWindow", "Green", nullptr));
        RAR_7->setText(QApplication::translate("MainWindow", "Red", nullptr));
        RAB_7->setText(QApplication::translate("MainWindow", "Blue", nullptr));
        RAY_8->setText(QApplication::translate("MainWindow", "Yellow", nullptr));
        RAG_8->setText(QApplication::translate("MainWindow", "Green", nullptr));
        RAR_8->setText(QApplication::translate("MainWindow", "Red", nullptr));
        RAB_8->setText(QApplication::translate("MainWindow", "Blue", nullptr));
        RAY_9->setText(QApplication::translate("MainWindow", "Yellow", nullptr));
        RAG_9->setText(QApplication::translate("MainWindow", "Green", nullptr));
        RAR_9->setText(QApplication::translate("MainWindow", "Red", nullptr));
        RAB_9->setText(QApplication::translate("MainWindow", "Blue", nullptr));
        RAY_10->setText(QApplication::translate("MainWindow", "Yellow", nullptr));
        RAG_10->setText(QApplication::translate("MainWindow", "Green", nullptr));
        RAR_10->setText(QApplication::translate("MainWindow", "Red", nullptr));
        RAB_10->setText(QApplication::translate("MainWindow", "Blue", nullptr));
        RAY_11->setText(QApplication::translate("MainWindow", "Yellow", nullptr));
        RAG_11->setText(QApplication::translate("MainWindow", "Green", nullptr));
        RAR_11->setText(QApplication::translate("MainWindow", "Red", nullptr));
        RAB_11->setText(QApplication::translate("MainWindow", "Blue", nullptr));
        RAY_12->setText(QApplication::translate("MainWindow", "Yellow", nullptr));
        RAG_12->setText(QApplication::translate("MainWindow", "Green", nullptr));
        RAR_12->setText(QApplication::translate("MainWindow", "Red", nullptr));
        RAB_12->setText(QApplication::translate("MainWindow", "Blue", nullptr));
        RAY_13->setText(QApplication::translate("MainWindow", "Yellow", nullptr));
        RAG_13->setText(QApplication::translate("MainWindow", "Green", nullptr));
        RAR_13->setText(QApplication::translate("MainWindow", "Red", nullptr));
        RAB_13->setText(QApplication::translate("MainWindow", "Blue", nullptr));
        RAY_14->setText(QApplication::translate("MainWindow", "Yellow", nullptr));
        RAG_14->setText(QApplication::translate("MainWindow", "Green", nullptr));
        RAR_14->setText(QApplication::translate("MainWindow", "Red", nullptr));
        RAB_14->setText(QApplication::translate("MainWindow", "Blue", nullptr));
        RAY_15->setText(QApplication::translate("MainWindow", "Yellow", nullptr));
        RAG_15->setText(QApplication::translate("MainWindow", "Green", nullptr));
        RAR_15->setText(QApplication::translate("MainWindow", "Red", nullptr));
        RAB_15->setText(QApplication::translate("MainWindow", "Blue", nullptr));
        RAY_16->setText(QApplication::translate("MainWindow", "Yellow", nullptr));
        RAG_16->setText(QApplication::translate("MainWindow", "Green", nullptr));
        RAR_16->setText(QApplication::translate("MainWindow", "Red", nullptr));
        RAB_16->setText(QApplication::translate("MainWindow", "Blue", nullptr));
        RAY_17->setText(QApplication::translate("MainWindow", "Yellow", nullptr));
        RAG_17->setText(QApplication::translate("MainWindow", "Green", nullptr));
        RAR_17->setText(QApplication::translate("MainWindow", "Red", nullptr));
        RAB_17->setText(QApplication::translate("MainWindow", "Blue", nullptr));
        RAY_18->setText(QApplication::translate("MainWindow", "Yellow", nullptr));
        RAG_18->setText(QApplication::translate("MainWindow", "Green", nullptr));
        RAR_18->setText(QApplication::translate("MainWindow", "Red", nullptr));
        RAB_18->setText(QApplication::translate("MainWindow", "Blue", nullptr));
        bt_image->setText(QApplication::translate("MainWindow", "ChangeBackground", nullptr));
        bt_save->setText(QApplication::translate("MainWindow", "Save", nullptr));
        bt_close->setText(QApplication::translate("MainWindow", "Close", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
