#include "mainwindow.h"
#include "ui_mainwindow.h"

const static int maxPosts = 18;

const static QString OnlineJson = "http://www.roboparty.org/config.json";
const static QString OnlineJsonSave = "http://www.roboparty.org/config.php";
const static QString OnlineImageSave = "http://www.roboparty.org/image.php";

const static QString wallpaper = "back.png";
const static QString result_image = "result_image.png";

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
   ui->setupUi(this);

   if (!QFile::exists(QDir::currentPath()+"/"+wallpaper))
   {
       QString fileName = QFileDialog::getOpenFileName(this, ("Chose a PNG image to Background 1920*1080"), QDir::currentPath(), ("Images (*.png)"));
       QFile::copy(fileName, QDir::currentPath()+"/"+wallpaper);
   }

   QPixmap bg(wallpaper);
   QPalette p(palette());
   p.setBrush(QPalette::Background, bg);
   setAutoFillBackground(true);
   setPalette(p);



   if(QSysInfo::productType()== "windows") qDebug() << "Windows version";
   else if(QSysInfo::productType()== "ubuntu")  qDebug() << "Ubuntu Version";
   else qDebug() << "productType():" << QSysInfo::productType();

   for(int i=0;i<maxPosts;i++) atributeWidgets(i);

   nam = new QNetworkAccessManager(this);
   QObject::connect(nam, SIGNAL(finished(QNetworkReply*)),this, SLOT(serviceRequestFinished(QNetworkReply*)));

   nam2 = new QNetworkAccessManager(this);
   QObject::connect(nam2, SIGNAL(finished(QNetworkReply*)),this, SLOT(serviceRequestFinished2(QNetworkReply*)));

   QUrl url(OnlineJson);
   nam->get(QNetworkRequest(url));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::atributeWidgets(int num)
{
    switch (num) {
    case 1:
        RadioButtonsYellow[num] = ui->RAY_1;
        RadioButtonsRed[num] = ui->RAR_1;
        RadioButtonsGreen[num] = ui->RAG_1;
        RadioButtonsBlue[num] = ui->RAB_1;
        TXT[num] = ui->TB_1;
        break;
    case 0:
        RadioButtonsYellow[num] = ui->RAY_2;
        RadioButtonsRed[num] = ui->RAR_2;
        RadioButtonsGreen[num] = ui->RAG_2;
        RadioButtonsBlue[num] = ui->RAB_2;
        TXT[num] = ui->TB_2;
        break;
    case 3:
        RadioButtonsYellow[num] = ui->RAY_3;
        RadioButtonsRed[num] = ui->RAR_3;
        RadioButtonsGreen[num] = ui->RAG_3;
        RadioButtonsBlue[num] = ui->RAB_3;
        TXT[num] = ui->TB_3;
        break;
    case 2:
        RadioButtonsYellow[num] = ui->RAY_4;
        RadioButtonsRed[num] = ui->RAR_4;
        RadioButtonsGreen[num] = ui->RAG_4;
        RadioButtonsBlue[num] = ui->RAB_4;
        TXT[num] = ui->TB_4;
        break;
    case 5:
        RadioButtonsYellow[num] = ui->RAY_5;
        RadioButtonsRed[num] = ui->RAR_5;
        RadioButtonsGreen[num] = ui->RAG_5;
        RadioButtonsBlue[num] = ui->RAB_5;
        TXT[num] = ui->TB_5;
        break;
    case 4:
        RadioButtonsYellow[num] = ui->RAY_6;
        RadioButtonsRed[num] = ui->RAR_6;
        RadioButtonsGreen[num] = ui->RAG_6;
        RadioButtonsBlue[num] = ui->RAB_6;
        TXT[num] = ui->TB_6;
        break;
    case 7:
        RadioButtonsYellow[num] = ui->RAY_7;
        RadioButtonsRed[num] = ui->RAR_7;
        RadioButtonsGreen[num] = ui->RAG_7;
        RadioButtonsBlue[num] = ui->RAB_7;
        TXT[num] = ui->TB_7;
        break;
    case 6:
        RadioButtonsYellow[num] = ui->RAY_8;
        RadioButtonsRed[num] = ui->RAR_8;
        RadioButtonsGreen[num] = ui->RAG_8;
        RadioButtonsBlue[num] = ui->RAB_8;
        TXT[num] = ui->TB_8;
        break;
    case 9:
        RadioButtonsYellow[num] = ui->RAY_9;
        RadioButtonsRed[num] = ui->RAR_9;
        RadioButtonsGreen[num] = ui->RAG_9;
        RadioButtonsBlue[num] = ui->RAB_9;
        TXT[num] = ui->TB_9;
        break;
    case 8:
        RadioButtonsYellow[num] = ui->RAY_10;
        RadioButtonsRed[num] = ui->RAR_10;
        RadioButtonsGreen[num] = ui->RAG_10;
        RadioButtonsBlue[num] = ui->RAB_10;
        TXT[num] = ui->TB_10;
        break;
    case 11:
        RadioButtonsYellow[num] = ui->RAY_11;
        RadioButtonsRed[num] = ui->RAR_11;
        RadioButtonsGreen[num] = ui->RAG_11;
        RadioButtonsBlue[num] = ui->RAB_11;
        TXT[num] = ui->TB_11;
        break;
    case 10:
        RadioButtonsYellow[num] = ui->RAY_12;
        RadioButtonsRed[num] = ui->RAR_12;
        RadioButtonsGreen[num] = ui->RAG_12;
        RadioButtonsBlue[num] = ui->RAB_12;
        TXT[num] = ui->TB_12;
        break;
    case 13:
        RadioButtonsYellow[num] = ui->RAY_13;
        RadioButtonsRed[num] = ui->RAR_13;
        RadioButtonsGreen[num] = ui->RAG_13;
        RadioButtonsBlue[num] = ui->RAB_13;
        TXT[num] = ui->TB_13;
        break;
    case 12:
        RadioButtonsYellow[num] = ui->RAY_14;
        RadioButtonsRed[num] = ui->RAR_14;
        RadioButtonsGreen[num] = ui->RAG_14;
        RadioButtonsBlue[num] = ui->RAB_14;
        TXT[num] = ui->TB_14;
        break;
    case 15:
        RadioButtonsYellow[num] = ui->RAY_15;
        RadioButtonsRed[num] = ui->RAR_15;
        RadioButtonsGreen[num] = ui->RAG_15;
        RadioButtonsBlue[num] = ui->RAB_15;
        TXT[num] = ui->TB_15;
        break;
    case 14:
        RadioButtonsYellow[num] = ui->RAY_16;
        RadioButtonsRed[num] = ui->RAR_16;
        RadioButtonsGreen[num] = ui->RAG_16;
        RadioButtonsBlue[num] = ui->RAB_16;
        TXT[num] = ui->TB_16;
        break;
    case 17:
        RadioButtonsYellow[num] = ui->RAY_17;
        RadioButtonsRed[num] = ui->RAR_17;
        RadioButtonsGreen[num] = ui->RAG_17;
        RadioButtonsBlue[num] = ui->RAB_17;
        TXT[num] = ui->TB_17;
        break;
    case 16:
        RadioButtonsYellow[num] = ui->RAY_18;
        RadioButtonsRed[num] = ui->RAR_18;
        RadioButtonsGreen[num] = ui->RAG_18;
        RadioButtonsBlue[num] = ui->RAB_18;
        TXT[num] = ui->TB_18;
        break;
    default:
        break;
    }

}

void MainWindow::paintEvent(QPaintEvent *){

    if(save)
    {
        QImage resultImage(wallpaper);
        QImage sourceImage(wallpaper);

        QPainter painter(&resultImage);
        painter.setCompositionMode(QPainter::CompositionMode_Source);
        painter.fillRect(resultImage.rect(), Qt::transparent);
        painter.setCompositionMode(QPainter::CompositionMode_SourceOver);

         QImage destinationImage;
        int x_impar = 1395;
        int y_impar = 34;

        int x_par = 1650;
        int y_par = 34;
        QFont font;

        int ySize = 99;
        int fontSize = 14;

        int txtStep1 = 30;
        int txtStep2 = 50;
        int txtStep3 = 75;

        int xStep = 10;

        //qDebug()  << "data_txt.count()" << data_txt.count();

        for(int i=0;i<data_txt.count();i++)
        {
            if(data_colors[i].toString()=="red") destinationImage.load(":/post_red.png");
            else if(data_colors[i].toString()=="green") destinationImage.load(":/post_green.png");
            else if(data_colors[i].toString()=="blue") destinationImage.load(":/post_blue.png");
            else  destinationImage.load(":/post_yellow.png");
            if(i%2)
            {
                painter.drawImage(x_par, y_par, destinationImage);
                font.setPixelSize(fontSize);
                font.setBold(true);
                painter.setFont(font);
                painter.drawText(x_par+xStep,y_par+txtStep1,data_taker[i].toString());
                font.setBold(false);
                painter.setFont(font);
                painter.drawText(x_par+xStep,y_par+txtStep2,data_txt[i].toString());
                painter.drawText(x_par+xStep,y_par+txtStep3,data_time[i].toString());
                y_par = y_par + ySize;
            }
            else
            {
                if(i==data_txt.count()-1)//Se último é ímpar
                {
                    painter.drawImage(x_par, y_par, destinationImage);
                    font.setPixelSize(fontSize);
                    font.setBold(true);
                    painter.setFont(font);
                    painter.drawText(x_par+xStep,y_par+txtStep1,data_taker[i].toString());
                    font.setBold(false);
                    painter.setFont(font);
                    painter.drawText(x_par+xStep,y_par+txtStep2,data_txt[i].toString());
                    painter.drawText(x_par+xStep,y_par+txtStep3,data_time[i].toString());
                    y_par = y_par + ySize;
                }
                else
                {
                    painter.drawImage(x_impar, y_impar, destinationImage);
                    font.setPixelSize(fontSize);
                    font.setBold(true);
                    painter.setFont(font);
                    painter.drawText(x_impar+xStep,y_impar+txtStep1,data_taker[i].toString());
                    font.setBold(false);
                    painter.setFont(font);
                    painter.drawText(x_impar+xStep,y_impar+txtStep2,data_txt[i].toString());
                    painter.drawText(x_impar+xStep,y_impar+txtStep3,data_time[i].toString());
                    y_impar = y_impar + ySize;
                }

            }
        }

        painter.setCompositionMode(QPainter::CompositionMode_DestinationOver);
        painter.drawImage(resultImage.rect(), sourceImage);

        painter.setCompositionMode(QPainter::CompositionMode_DestinationOver);
        painter.fillRect(resultImage.rect(), Qt::white);
        painter.end();
        resultImage.save(result_image);

        save = false;

        sendfile();

    }

}

void MainWindow::on_bt_save_clicked()
{
    QStringList taker,task,time,colors;

    data_taker = QJsonArray{};
    data_txt = QJsonArray{};
    data_time = QJsonArray{};
    data_colors = QJsonArray{};

    //qDebug()  << "data_txt.count() save" << data_txt.count();

    for(int i=0;i<maxPosts;i++)
    {
       QString txt = TXT[i]->toPlainText();
       if(txt!="")
       {
         QStringList pieces = txt.split( "\n" );
         if(pieces.length()==3)
         {
             taker.append(pieces.value(0));
             data_taker.append(pieces.value(0));
             //data_taker[i] = pieces.value(0);
             task.append(pieces.value(1));
             data_txt.append(pieces.value(1));
             //data_txt[i] = pieces.value(1);
             time.append(pieces.value(2));
             data_time.append(pieces.value(2));
             //data_time[i] = pieces.value(2);
         }
         if(RadioButtonsRed[i]->isChecked()){
             data_colors.append("red");
             colors.append("red");
         }
         else if(RadioButtonsGreen[i]->isChecked()){
             data_colors.append("green");
             colors.append("green");
         }
         else if(RadioButtonsBlue[i]->isChecked())
         {
             data_colors.append("blue");
             colors.append("blue");
         }
         else{
             data_colors.append("yellow");
             colors.append("yellow");
         }
       }
    }

    QString data; // assume this holds the json string
    data = "{\"desktop\": {\"version\": \"0.01\"";

    data += ",\"taker\":[";
    for(int i=0;i<taker.count();i++)
    {
        data += "\""+taker.at(i)+"\",";
    }
    data.remove(data.length()-1,1);
    data += "]";

    data += ",\"task\":[";
    for(int i=0;i<task.count();i++)
    {
        data += "\""+task.at(i)+"\",";
    }
    data.remove(data.length()-1,1);
    data += "]";

    data += ",\"time\":[";
    for(int i=0;i<time.count();i++)
    {
        data += "\""+time.at(i)+"\",";
    }
    data.remove(data.length()-1,1);
    data += "]";

    data += ",\"colors\":[";
    for(int i=0;i<colors.count();i++)
    {
        data += "\""+colors.at(i)+"\",";
    }
    data.remove(data.length()-1,1);
    data += "]";

    data += "}}";

    QJsonDocument doc = QJsonDocument::fromJson(data.toUtf8());
    QFile jsonFile("config.json");
    jsonFile.open(QFile::WriteOnly);
    jsonFile.write(doc.toJson());

    QImage resultImage(wallpaper);
    QUrl url(OnlineJsonSave+"?json="+data);
    nam2->get(QNetworkRequest(url));

    execComand();

    save = true;

}

void MainWindow::on_bt_close_clicked()
{
    QCoreApplication::quit();
}

void MainWindow::serviceRequestFinished(QNetworkReply *reply)
{
    if(reply->error() == QNetworkReply::NoError) {

           QStringList propertyNames;
           QStringList propertyKeys;

           QString strReply = reply->readAll();

           //qDebug() << strReply;

           QJsonDocument d = QJsonDocument::fromJson(strReply.toUtf8());
           QJsonObject sett2 = d.object();
           QJsonValue value = sett2.value(QString("desktop"));
           QJsonObject item = value.toObject();

           QJsonValue version = item["version"];
           qWarning() << "version:" << version.toString();

           data_taker = item["taker"].toArray();
           data_txt = item["task"].toArray();
           data_colors = item["colors"].toArray();
           data_time = item["time"].toArray();

           for(int i=0;i<data_txt.count();i++)
           {
             TXT[i]->appendPlainText(data_taker[i].toString()+"\n"+data_txt[i].toString()+"\n"+data_time[i].toString());
             if(data_colors[i].toString()=="red") RadioButtonsRed[i]->setChecked(true);
             else if(data_colors[i].toString()=="green") RadioButtonsGreen[i]->setChecked(true);
             else if(data_colors[i].toString()=="blue") RadioButtonsBlue[i]->setChecked(true);
             else  RadioButtonsYellow[i]->setChecked(true);
           }

       } else {
           qDebug() << "ERROR";
       }

    delete reply;
}

void MainWindow::serviceRequestFinished2(QNetworkReply *reply)
{
    if(reply->error() == QNetworkReply::NoError)
    {
           QStringList propertyNames;
           QStringList propertyKeys;
           QString strReply = reply->readAll();
           //qDebug() << "saving" << strReply;
    }
    else qDebug() << "ERROR";

    delete reply;
}

void MainWindow::sendfile()
{
    QUrl testUrl(OnlineImageSave);
    QNetworkRequest request(testUrl);
    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

    QHttpPart textPart;
     textPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"text\""));
     textPart.setBody("Merhaba ");

     QStringList headers;
     headers
         << "Accept  text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
         << "Accept-Encoding	gzip, deflate"
         << "Accept-Language	ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3"
         << "User-Agent	Mozilla/5.0 (X11; Linux x86_64; rv:20.0) Gecko/20100101 Firefox/20.0";

    request.setRawHeader("Content-ID", "12345");
    request.setRawHeader("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
    request.setRawHeader("Accept-Encoding",	"gzip, deflate");
    request.setRawHeader("Accept-Language",	"ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");
    request.setRawHeader("User-Agent",	"Mozilla/5.0 (X11; Linux x86_64; rv:20.0) Gecko/20100101 Firefox/20.0");
    //request.setRawHeader("User-Agent", "Mozilla/5.21");
    QFile *file = new QFile(result_image);
    qDebug() << file->open(QIODevice::ReadOnly);
    QFileInfo fileInfo(result_image);
    QHttpPart imagePart;
    imagePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("image/png"));
    imagePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant(QString("form-data; name=\"file\";filename=\"%1\"").arg(fileInfo.fileName()).toLatin1()) );
    imagePart.setBodyDevice(file);
    file->setParent(multiPart);
    multiPart->append(textPart);
    multiPart->append(imagePart);

    QNetworkAccessManager *manager = new QNetworkAccessManager;     //using qnetwork access manager for post data

    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(erroron_filesend(QNetworkReply*)));    //connecting manager object for errors here
    manager->post(request,multiPart); //send all data

}

void MainWindow::execComand()
{
    if(QSysInfo::productType()== "windows")
    {
        QProcess process;
        process.setProgram( "cmd.exe" );
        process.setArguments( { "/C", "("+QDir::currentPath()+"/wallpaper.bat "+QDir::currentPath()+")" } );
        process.setWorkingDirectory( R"(C:\)" );
        process.setStandardOutputFile( QProcess::nullDevice() );
        process.setStandardErrorFile( QProcess::nullDevice() );
        process.startDetached();
    }
    else if(QSysInfo::productType()== "ubuntu")
    {
        QProcess::execute("gsettings set org.gnome.desktop.background picture-uri http://www.roboparty.org/result_image.png");
    }


    QString message_d = QString("Upload Complete");
    QMessageBox::information(this, "Upload Complete", message_d);
}

void MainWindow::erroron_filesend(QNetworkReply *replye)
{
    if (replye->error() !=0)
    {
        QMessageBox::information(this,"Connection Error",replye->errorString());
        return;
    }
    else
    {
        execComand();
        return;
    }
}

void MainWindow::on_bt_image_clicked()
{
    if (QFile::exists(QDir::currentPath()+"/"+wallpaper))
    {
        QFile::remove(QDir::currentPath()+"/"+wallpaper);
    }

    QString fileName = QFileDialog::getOpenFileName(this, ("Open File"), QDir::currentPath(), ("Images (*.png)"));
    QFile::copy(fileName, QDir::currentPath()+"/"+wallpaper);

    qApp->quit();
    QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
}
