#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPainter>
#include <QRect>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QDebug>
#include <QRadioButton>
#include <QPlainTextEdit>
#include <QFileDialog>
#include <QProcess>
#include <QUrl>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QMessageBox>
#include <QHttpPart>
#include <QSysInfo>
#include <QDir>
#include <QGraphicsEffect>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    void atributeWidgets(int num);

    void on_bt_save_clicked();

    void on_bt_close_clicked();

    void serviceRequestFinished(QNetworkReply* reply);

    void serviceRequestFinished2(QNetworkReply* reply);

    void erroron_filesend(QNetworkReply *replye);

    void sendfile();

    void execComand();

    void on_bt_image_clicked();

protected:
    void paintEvent(QPaintEvent *);

private:
    Ui::MainWindow *ui;

    QPlainTextEdit *TXT[18];
    QRadioButton *RadioButtonsYellow[18];
    QRadioButton *RadioButtonsRed[18];
    QRadioButton *RadioButtonsGreen[18];
    QRadioButton *RadioButtonsBlue[18];
    QJsonArray data_taker;
    QJsonArray data_txt;
    QJsonArray data_colors;
    QJsonArray data_time;

    bool save = false;
    QNetworkAccessManager *nam;
    QNetworkReply* reply;

    QNetworkAccessManager *nam2;
    QNetworkReply* reply2;
};

#endif // MAINWINDOW_H
